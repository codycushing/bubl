var mongojs = require("mongojs");
var db = mongojs.connect("mongodb://bubluser:howmanytimes6@flame.mongohq.com:27038/bubl", ["projects", "nodes"]);


function index(response, request) {
	console.log("ProjectController.index");
	
	db.projects.find(function(err, docs) {	
		send( response, docs );
	});
}

function create(response, request, postData) {
	console.log("ProjectController.create");
	
	var project = { name: postData.projectName, description: postData.projectDescription };

	db.projects.save(project, function(err, saved) {
		if( err || !saved )
			send( response, {status: "fail"} );
		else 
			send( response, {status: "success"} );
	});
}

function read(response, request, getData) {
	console.log("ProjectController.read");
	
	db.projects.findOne( { _id: mongojs.ObjectId(getData.id) }, function(err, project) {

		db.nodes.find( { parentId: mongojs.ObjectId(getData.id) }, function(err, nodes) {
			var data = { project: project, nodes: nodes };
			send( response, data );
		} );
		
	} );
}

function update(response, request, postData) {
	console.log("ProjectController.update");
	
    var obj = { $set: {} };
    obj.$set[postData.name] = postData.value;
    db.projects.update( { _id: mongojs.ObjectId(postData.id) }, obj );
    
	send( response, {status:"success"} );
}

function del(response, request, getData) {
	console.log("ProjectController.del");
	
	db.projects.remove( { _id: mongojs.ObjectId(getData.id) } );
	
	send( response, {status:"success"} );
}

function nukedb(response) {
	console.log("ProjectController.nukedb");
	
	db.projects.remove();
	db.nodes.remove();
	
	send( response, {status:"success"} );
}

function send(response, data) {
	response.writeHead(200, {'content-type': 'text/json' });
	response.write( JSON.stringify(data) );
	response.end('\n');
}


exports.index = index;
exports.create = create;
exports.read = read;
exports.update = update;
exports.del = del;
exports.nukedb = nukedb;

