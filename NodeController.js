var mongojs = require("mongojs");
var db = mongojs.connect("mongodb://bubluser:howmanytimes6@flame.mongohq.com:27038/bubl", ["nodes"]);


function index(response, request, getData) {
	console.log("NodeController.index");
	
	db.nodes.find(function(err, docs) {
		send( response, docs );
	});
}

function create(response, request, postData) {
	console.log("NodeController.create");

	if(postData.parentId !== undefined)
		postData.parentId = mongojs.ObjectId(postData.parentId);

	db.nodes.save(postData, function(err, saved) {
		if( err || !saved )
			send( response, {status: "fail"} );
		else 
			send( response, {status: "success"} );
	});
}

function read(response, request, getData) {
	console.log("NodeController.read");
	
	db.nodes.findOne( { _id: mongojs.ObjectId(getData.id) }, function(err, node) {

		db.nodes.find( { parentId: mongojs.ObjectId(getData.id) }, function(err, nodes) {
			var data = { node: node, nodes: nodes };
			send( response, data );
		} );
	} );
}

function update(response, request, postData) {
	console.log("NodeController.update");
    
    var obj = { $set: {} };
    obj.$set[postData.name] = postData.value;
    db.nodes.update( { _id: mongojs.ObjectId(postData.id) }, obj );
    
	send( response, {status: "success"} );
}

function del(response, request, getData) {
	console.log("NodeController.del");
	
	db.nodes.remove( { _id: mongojs.ObjectId(getData.id) } );
	send( response, {status: "success"} );
}

function nukedb(response) {
	console.log("ProjectController.nukedb");
	
	db.nodes.remove();
	send( response, {status: "success"} );
}

function send(response, data) {
	response.writeHead(200, {'content-type': 'text/json' });
	response.write( JSON.stringify(data) );
	response.end('\n');
}


exports.index = index;
exports.create = create;
exports.read = read;
exports.update = update;
exports.del = del;
exports.nukedb = nukedb;