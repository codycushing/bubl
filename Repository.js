// ajax proxy for asynchronous data manipulation

var BUBL = BUBL || {};

BUBL.Repository = function(){
    "use strict";
    
    var Promise = BUBL.Promise;
    
    var Repository = function(){
        
    };
    
    Repository.prototype.listProjects = function () {
        var promise = new Promise();
        
        $.ajax({ url: '/project/index', contentType: 'text/json', 
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.createProject = function (postData) {
        var promise = new Promise();
        
        $.ajax({ url: '/project/create', contentType: 'text/json', type: "POST", data: postData,
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.readProject = function (projectId) {
        var promise = new Promise();
        
        $.ajax({ url: '/project/read', contentType: 'text/json', type: "GET", data: {id: projectId}, 
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.deleteProject = function (projectId) {
        var promise = new Promise();
        
        $.ajax({ url: '/project/delete', contentType: 'text/json', type: "POST", data: {id: projectId}, 
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.createItem = function (postData) {
        var promise = new Promise();
        
        $.ajax({ url: '/node/create', contentType: 'text/json', type: "POST", data: postData, 
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.readItem = function (itemId) {
        var promise = new Promise();
        
        $.ajax({ url: '/node/read', contentType: 'text/json', data: {id: itemId}, 
            success: function(data){ promise.resolve(data); }
        });
        
        return promise;
    };
    
    Repository.prototype.deleteItem = function (itemId) {
        var promise = new Promise();
        
        $.ajax({ url: '/node/delete', contentType: 'text/json', type: "POST", data: {id: itemId}, 
            success: function(){ promise.resolve(); }
        });
    
        return promise;
    };
    
    Repository.prototype.updateProperty = function (controller, data) {
        var promise = new Promise();
        
        $.ajax({ url: '/'+controller+'/update', contentType: 'text/json', type: "POST", data: data, 
            success: function(){ promise.resolve(); }
        });
        
        return promise;
    };
    
    return Repository;
}();