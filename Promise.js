// Promise - Allows syntactically streamlined callback code using inline 'then' and 'fail' function declarations 

var BUBL = BUBL || {};

BUBL.Promise = (function () {

// === constructor and instance members ===
var Promise = function (optionalDescription) {
    this.description = optionalDescription;
    this.readyState = Promise.UNINITIALIZED;
    this.result = null;
    this.thenCallback = null;
    this.failCallback = null;
};

// === prototype members ===
Promise.prototype.resolve = function (args) {
    if (!this.thenCallback && this.readyState == Promise.UNINITIALIZED)
        throw new Error("'then' callback was never set, code may have executed synchronously before 'then' was set");

    this.result = arguments;
    this.readyState = Promise.DONE;
    this.thenCallback.apply(null, arguments);
};

Promise.prototype.error = function (args) {
    if (!this.failCallback)
        throw new Error("'fail' callback was never set, code may have executed synchronously before 'fail' was set");

    this.readyState = Promise.ERROR;
    this.failCallback.apply(null, arguments);
};

Promise.prototype.then = function (thenCallback) {
    this.readyState = Promise.PENDING;
    this.thenCallback = thenCallback;
    return this;
};

Promise.prototype.fail = function (failCallback) {
    this.readyState = Promise.ERROR;
    this.failCallback = failCallback;
    return this;
};

// === static members ===
Promise.UNINITIALIZED = "0";
Promise.PENDING = "1";
Promise.DONE = "2";
Promise.ERROR = "3";

return Promise;
})();