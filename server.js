var http = require("http");
var url = require("url");

var router = require("./Router");
var HomeController = require("./HomeController");
var ProjectController = require("./ProjectController");
var NodeController = require("./NodeController");


var routeMap = {};
routeMap["/"] = HomeController.index;
routeMap["/project/index"] = ProjectController.index;
routeMap["/project/create"] = ProjectController.create;
routeMap["/project/read"] = ProjectController.read;
routeMap["/project/update"] = ProjectController.update;
routeMap["/project/delete"] = ProjectController.del;
routeMap["/project/nukedb"] = ProjectController.nukedb;

routeMap["/node/"] = NodeController.index;
routeMap["/node/index"] = NodeController.index;
routeMap["/node/create"] = NodeController.create;
routeMap["/node/read"] = NodeController.read;
routeMap["/node/update"] = NodeController.update;
routeMap["/node/delete"] = NodeController.del;
routeMap["/node/nukedb"] = NodeController.nukedb;


var onRequest = function(request, response) {
	var pathname = url.parse(request.url).pathname;
	router.route(routeMap, pathname, response, request);
};

http.createServer(onRequest).listen(process.env.PORT, "0.0.0.0");

