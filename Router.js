var fs = require('fs');
var qs = require('querystring');
var path = require('path');

function handleStatic(routeMap, pathname, response, request){
	var filePath = '.' + request.url;
	if (filePath == './')
		filePath = './index.html';
		 
	var extname = path.extname(filePath);
	var contentType = 'text/html';
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
	}
	 
	path.exists(filePath, function(exists) {
	 
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200, { 'Content-Type': contentType });
					response.end(content, 'utf-8');
				}
			});
		}
		else {
			console.log("No file found: " + pathname);
			response.writeHead(404);
			response.end();
		}
	});
}

function handleJson(routeMap, pathname, response, request){
	
	if (typeof routeMap[pathname] === 'function') {
		if(request.method  == "POST") {
			var body = '';
			
			request.on('data', function (data) {
				body += data;
			});
			
			request.on('end', function () {
				var postData = qs.parse(body);
				routeMap[pathname](response, request, postData);
			});
		}
		else {
			var str = request.url;
			str = str.replace(/.*\/?\?/, ""); // matches word/?name or word?name
			var getData = qs.parse(str);
			routeMap[pathname](response, request, getData);
		}
	}
	else {
		console.log("No route mapping for: " + pathname);
		response.writeHead(404, {"Content-Type": "text/html"});
		response.write("404 Not found");
		response.end();
	}
}

function route(routeMap, pathname, response, request) {
	
	if( request.headers['content-type'] == 'text/json')
		handleJson(routeMap, pathname, response, request);
	else
		handleStatic(routeMap, pathname, response, request);
}

exports.route = route;