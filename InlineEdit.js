/* This plugin will handle clicks on any element with a 'mapsTo' attribute and replace it with a prepopulated input. 
When the input loses focus, it will be restored to the previous element, and the value will be updated.
If an 'onSave' method was passed into the inlineEdit constructor, it will be invoked if the value has changed from the original.

The 'onSave' method requires the following signature: function(id, mapsTo, value) where:
  'id' is the element's id,
  'mapsTo' is whatever value the mapsTo attribute stored, and
  'value' is the new value to be saved
*/
(function($) {
    
    $.fn.inlineEdit = function(onSave) {
        var context = this;
        context._onSave = onSave;
        context._input = document.createElement('input');
        context._input.addEventListener('focusout', function() { 
            endInlineEdit.call(this, context);
        });
        
        this.on('click', "[mapsTo]", function() { 
            beginInlineEdit.call(this, context);
        });
    };
    
    function beginInlineEdit(context){
        context._original = this;
        context._input.className = this.className;
        context._input.value = $(this).html();
        $(this).replaceWith(context._input);
        context._input.focus();
    }
    
    function endInlineEdit(context){
        var updated = $(this).val();

        $(this).replaceWith( $(context._original) );
        
        if($(context._original).html() != updated){
            $(context._original).html(updated); // only update the original element's text if it has changed
            // skip if no onSave method was passed in
            context._onSave && context._onSave.call(context._original, updated ); // set 'this' to the original element
        }
    }
    
})( jQuery );