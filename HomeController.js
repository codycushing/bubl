var querystring = require("querystring");
var fs = require("fs");

function index(response) {
	console.log("HomeController.index");

	fs.readFile('index.html', function(error, content) {
		if (error) {
			console.log(error);
			response.writeHead(500);
			response.end();
		}
		else {
			console.log("ProjectController: serving index.html");
			response.writeHead(200, { 'Content-Type': 'text/html' });
			response.end(content, 'utf-8');
		}
	});

}

function show(response) {

}

exports.index = index;
exports.show = show;
